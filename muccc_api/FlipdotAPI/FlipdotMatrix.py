import socket

from .font import font8px


class FlipdotImage:
    BLACKPIXEL = 0
    WHITEPIXEL = 1
    width: int
    height: int
    rowArrayOfLineArraysOfPixels: list[list[int]]

    def __init__(self, pixel2DArray: list[list[int]]):
        self.width = len(pixel2DArray[0])
        self.height = len(pixel2DArray)
        self.rowArrayOfLineArraysOfPixels = pixel2DArray

    def blitImageAtPosition(self, flipdotImage: "FlipdotImage", xPos: int = 0, yPos: int = 0):
        for lineNr in range(self.height):
            newImageLineNr = lineNr - yPos
            if newImageLineNr >= 0 and flipdotImage.height > newImageLineNr:
                self.__blitLineAtPosition(flipdotImage, lineNr, newImageLineNr, xPos, yPos)

    def __blitLineAtPosition(
        self, flipdotImage: "FlipdotImage", lineNr: int, newImageLineNr: int, xPos: int, yPos: int
    ):
        for rowNr in range(self.width):
            newImageRowNr = rowNr - xPos
            if newImageRowNr >= 0 and flipdotImage.width > newImageRowNr:
                self.rowArrayOfLineArraysOfPixels[lineNr][rowNr] = (
                    flipdotImage.rowArrayOfLineArraysOfPixels[newImageLineNr][newImageRowNr]
                )

    def blitTextAtPosition(
        self,
        text: str,
        autoLineBreak: bool = False,
        xPos: int = 0,
        yPos: int = 0,
        __indentXPos: int | None = None,
        lineBreakAtWhiteSpace: bool = False,
    ):
        if __indentXPos is None:
            __indentXPos = xPos

        if len(text) <= 0:
            return

        letterImages: list[FlipdotImage] = [
            self.__getLetterImageForNextLetter(line) for line in text
        ]

        for i in range(len(text)):
            self.blitImageAtPosition(letterImages[i], xPos, yPos)

            if (
                autoLineBreak
                and i + 1 < len(letterImages)
                and self.__isLineBreakRequired(letterImages[i + 1], xPos)
            ) or (
                autoLineBreak
                and lineBreakAtWhiteSpace
                and self.__isWhitespaceLineBreakRequired(i + 1, text, letterImages, xPos)
            ):
                xPos = __indentXPos
                yPos = yPos + font8px.lineheight - 1

            else:
                xPos += letterImages[i].width

    def __isLineBreakRequired(self, letterImage: "FlipdotImage", xPos: int) -> bool:
        return letterImage.width > self.width - xPos

    def __isWhitespaceLineBreakRequired(
        self, i: int, text: str, letterImages: list["FlipdotImage"], xPos: int
    ) -> bool:
        width_to_next_whitespace = 0
        while i < len(letterImages) and text[i] != " ":
            width_to_next_whitespace += letterImages[i].width
            i += 1

        return width_to_next_whitespace > self.width - xPos

    def __getLetterImageForNextLetter(self, text: str) -> "FlipdotImage":
        nextLetter = text[:1].upper()
        image = font8px.letters.get(nextLetter, font8px.letters["?"])
        return FlipdotImage(image)

    def serializeImageArray(self) -> list[int]:
        imageArray: list[int] = []
        for y in range(self.height):
            for x in range(self.width):
                imageArray.append(self.rowArrayOfLineArraysOfPixels[y][x])
        return imageArray

    def getLine(self, line: int):
        return self.rowArrayOfLineArraysOfPixels[line]

    @classmethod
    def newBlackFlipdotImage(cls, width: int, height: int):
        pixel2DArray = cls.generateColoredRowArrayOfLineArraysOfPixels(
            width, height, FlipdotImage.BLACKPIXEL
        )
        return cls(pixel2DArray)

    @classmethod
    def newWhiteFlipdotImage(cls, width: int, height: int):
        pixel2DArray = cls.generateColoredRowArrayOfLineArraysOfPixels(
            width, height, FlipdotImage.WHITEPIXEL
        )
        return cls(pixel2DArray)

    @staticmethod
    def generateColoredRowArrayOfLineArraysOfPixels(width: int, height: int, color: int):
        rowArrayOfLineArrayOfPixels: list[list[int]] = []
        for _y in range(height):
            rowArrayOfLineArrayOfPixels.append(
                FlipdotImage.generateColoredLineArrayOfPixels(width, color)
            )
        return rowArrayOfLineArrayOfPixels

    @staticmethod
    def generateColoredLineArrayOfPixels(width: int, color: int) -> list[int]:
        lineArrayOfPixels: list[int] = []
        for _x in range(width):
            lineArrayOfPixels.append(color)
        return lineArrayOfPixels


class FlipdotMatrix:
    def __init__(
        self,
        udpHostAndPort: tuple[str, int] = ("2001:7f0:3003:beef:222:f9ff:fe01:c65", 2323),
        imageSize: tuple[int, int] = (40, 16),
    ):
        self.__sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.udpHostAndPort = udpHostAndPort
        self.flipdotImage = FlipdotImage.newBlackFlipdotImage(imageSize[0], imageSize[1])

    def resetAll(self):
        """
        flip every pixel at least once, end with cleared Matrix
        """
        width = self.flipdotImage.width
        height = self.flipdotImage.height
        whiteImage = FlipdotImage.newWhiteFlipdotImage(width, height)
        blackImage = FlipdotImage.newBlackFlipdotImage(width, height)
        self.show(whiteImage)
        self.show(blackImage)

    def __showSerializedArrayOfPixels(self, imageArray: list[int]):
        udpPacket = FlipdotMatrix.__arrayToPacket(imageArray)
        self.__sendUdpPackage(udpPacket)

    def show(self, image: FlipdotImage):
        """
        send FlipdotImage to display. fills up with black pixels
        """
        self.__clearFlipdotImageWithoutUpdate()
        self.flipdotImage.blitImageAtPosition(image)
        self.__updateFlipdotMatrix()

    def showBlit(self, image: FlipdotImage, xPos: int = 0, yPos: int = 0):
        """
        send FlipdotImage to display, keeps old pixels around
        """
        self.flipdotImage.blitImageAtPosition(image, xPos, yPos)
        self.__updateFlipdotMatrix()

    def __updateFlipdotMatrix(self):
        serializedImageArray = self.flipdotImage.serializeImageArray()
        self.__showSerializedArrayOfPixels(serializedImageArray)

    def clear(self):
        """
        clears display. fills with black pixels
        """
        self.__clearFlipdotImageWithoutUpdate()
        self.__updateFlipdotMatrix()

    def __clearFlipdotImageWithoutUpdate(self):
        width = self.flipdotImage.width
        height = self.flipdotImage.height
        self.flipdotImage = FlipdotImage.newBlackFlipdotImage(width, height)

    def showText(
        self,
        text: str,
        linebreak: bool = False,
        xPos: int = 0,
        yPos: int = 0,
        lineBreakAtWhiteSpace: bool = False,
    ):
        """
        print text to display
        """
        self.__clearFlipdotImageWithoutUpdate()
        self.flipdotImage.blitTextAtPosition(
            text, linebreak, xPos, yPos, lineBreakAtWhiteSpace=lineBreakAtWhiteSpace
        )
        self.__updateFlipdotMatrix()

    def showBlitText(self, text: str, linebreak: bool = False, xPos: int = 0, yPos: int = 0):
        """
        print text to display, keeps old pixels around
        """
        self.flipdotImage.blitTextAtPosition(text, linebreak, xPos, yPos)
        self.__updateFlipdotMatrix()

    @staticmethod
    def __arrayToPacket(imageArray: list[int]) -> bytearray:
        return bytearray([
            FlipdotMatrix.__list2byte(imageArray[i * 8 : i * 8 + 8])
            for i in range(int(len(imageArray) / 8))
        ])

    @staticmethod
    def __list2byte(ArrayOfBinaryInts: list[int]):
        byte = 0
        for i in range(8):
            byte += 2 ** (7 - i) if ArrayOfBinaryInts[i] else 0
        return byte

    def __sendUdpPackage(self, udpPacket: bytearray):
        self.__sock.sendto(udpPacket, self.udpHostAndPort)


# main
if __name__ == "__main__":
    # das kleine Panel
    matrix = FlipdotMatrix()
    matrix.resetAll()
    matrix.showText("flip the dots!", linebreak=True, lineBreakAtWhiteSpace=True)
    # das längere Panel
    zeile = FlipdotMatrix(
        udpHostAndPort=("2001:7f0:3003:beef:ba27:ebff:fe89:4cd2", 2323), imageSize=(176, 20)
    )
    zeile.showText(
        "The quick brown fox jumps over the lazy dog",
        linebreak=True,
        lineBreakAtWhiteSpace=True,
    )
