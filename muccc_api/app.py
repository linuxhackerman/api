import asyncio
from contextlib import asynccontextmanager
from pathlib import Path

import uvicorn
from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi
from fastapi.responses import RedirectResponse, Response
from fastapi.staticfiles import StaticFiles
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend
from fastapi_cache.coder import JsonCoder, PickleCoder
from fastapi_cache.decorator import cache
from prometheus_fastapi_instrumentator import Instrumentator as PrometheusInstrumentator

from .log import setup_logging
from .schleuse import SchleuseStateResponse, SchleuseUdpReceiver
from .spaceapi import SpaceAPIResponse, SpaceAPIState
from .wiki import Event, EventFilter, Wiki

schleuse = SchleuseUdpReceiver()
wiki = Wiki("https://wiki.muc.ccc.de")


@asynccontextmanager
async def lifespan(_: FastAPI):
    FastAPICache.init(backend=InMemoryBackend(), coder=PickleCoder)  # type: ignore
    schleuse.run_task()
    yield


app = FastAPI(lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
    max_age=3600 * 24,
)

PrometheusInstrumentator().instrument(app).expose(app)

app.mount(
    "/static",
    StaticFiles(directory=Path(__file__).parent / Path("static")),
    name="static",
)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="MuCCC API",
        version="0.2.0-dev",
        description="API for the MuCCC hackerspace.",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://api.muc.ccc.de/static/logo.png",
    }
    openapi_schema["externalDocs"] = {"url": "https://wiki.muc.ccc.de/api"}
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi  # type: ignore


@app.get("/", include_in_schema=False)
async def root():
    return RedirectResponse("/docs", status_code=302)


@app.get(
    "/spaceapi.json",
    summary="Get SpaceAPI data",
    tags=["spaceapi"],
    response_model=SpaceAPIResponse,
)
async def spaceapi():
    if schleuse.state is not None:
        msg = "members only" if schleuse.state.value == "member" else schleuse.state.value
    else:
        msg = "not available"

    return SpaceAPIResponse(
        state=SpaceAPIState(
            open=None if schleuse.state is None else schleuse.state in ["public", "member"],
            message=msg,
        )
    )


@app.get(
    "/schleuse.json",
    summary="Return the state of the Luftschleuse",
    tags=["schleuse"],
    response_model=SchleuseStateResponse,
)
async def hqstate(poll: bool = False):
    """
    Returns the state of the Luftschleuse as a JSON string.

    If parameter `poll` is true, this will make the request block indefinitely
    until the state changes and return the new state.
    """
    if poll:
        await schleuse.wait_for_state_change()
    return {"state": schleuse.state.value if schleuse.state is not None else None}


@app.get(
    "/events/all.ics",
    summary="All Events as ical feed",
    tags=["calendar"],
)
@cache(expire=60)
async def events():
    """
    All upcoming event events
    """
    return Response(content=wiki.ical(filter=EventFilter(public=False)), media_type="text/calendar")


@app.get(
    "/events/public.ics",
    summary="Public Events as ical feed",
    tags=["calendar"],
)
@cache(expire=60)
async def events_public():
    """
    All public upcoming event
    """
    return Response(content=wiki.ical(filter=EventFilter(public=True)), media_type="text/calendar")


@app.get(
    "/events/today",
    summary="Todays events as JSON",
    tags=["calendar"],
)
@cache(expire=60, coder=JsonCoder)
async def todays_events() -> list[Event]:
    """
    Legacy today's events as JSON for anzeigr in Hauptraum
    """
    return list(wiki.todays_events())


@app.get(
    "/events/upcoming",
    summary="Upcoming events as JSON",
    tags=["calendar"],
)
@cache(expire=60, coder=JsonCoder)
async def upcoming_events(limit: int = 10) -> list[Event]:
    """
    Legacy next event as JSON for anzeigr in Hauptraum
    """
    return [x for _, x in zip(range(limit), wiki.upcoming_events(), strict=False)]


@app.websocket("/schleuse.json/ws")
async def hqstate_ws(websocket: WebSocket):
    async def notify_websocket():
        while True:
            await websocket.send_json(await hqstate())
            await schleuse.wait_for_state_change()

    await websocket.accept()
    notify_task = asyncio.create_task(notify_websocket())

    try:
        async for _ in websocket.iter_text():
            pass  # Ignore incoming messages but react to disconnects
    except WebSocketDisconnect:
        await websocket.close()

    notify_task.cancel()


def run():
    server = uvicorn.Server(uvicorn.Config(app, host="::", port=8020))
    setup_logging()
    server.run()
