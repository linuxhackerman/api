import asyncio
import os
import socket
from collections.abc import Callable, Coroutine
from enum import Enum
from typing import Any

from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientConnectorError, WSServerHandshakeError
from aiohttp.http import WSMsgType
from anyio import Condition, create_udp_socket
from loguru import logger
from prometheus_client import Gauge
from pydantic import BaseModel

background_tasks: set[asyncio.Task[None]] = set()


class SchleuseStateEnum(str, Enum):
    down = "down"
    closed = "closed"
    member = "member"
    public = "public"


class SchleuseStateResponse(BaseModel):
    state: SchleuseStateEnum | None


class SchleuseUdpReceiver:
    state: SchleuseStateEnum | None
    state_gauge: Gauge
    state_last_updated_gauge: Gauge

    def __init__(self):
        self.state = None
        self._state_changed = Condition()
        self.state_gauge = Gauge("schleuse_state", "State of Luftschleuse", ["mode"])
        self.state_last_updated_gauge = Gauge(
            "schleuse_state_last_updated", "Unixtime when last Luftschleuse state change happened"
        )

    async def wait_for_state_change(self):
        async with self._state_changed:
            await self._state_changed.wait()

    def run_task(self):
        self.task = asyncio.create_task(self.listen_for_state_changes())
        self.task_supervisor = asyncio.create_task(self.supervise_task())

    @logger.catch(reraise=True)
    async def supervise_task(self):
        while True:
            await asyncio.wait([self.task], return_when=asyncio.FIRST_EXCEPTION)
            logger.error(f"Receiver task died: {self.task.exception()!r}")
            self.task = asyncio.create_task(self.listen_for_state_changes())
            logger.info("Restarted receiver task")

    @logger.catch(reraise=True)
    async def listen_for_state_changes(self):
        async with await create_udp_socket(
            family=socket.AF_INET6, local_host="::", local_port=2080
        ) as udp:
            logger.info("Listening on port 2080 for UDP schleuse datagrams")
            async for packet, (host, port) in udp:
                logger.debug(f"UDP datagram from {host}:{port} => {packet}")
                newstate = packet.decode("ascii").strip()
                if newstate != self.state:
                    try:
                        self.state = SchleuseStateEnum(newstate)
                        logger.info(f"schleuse.state: {self.state.value}")
                    except ValueError:
                        logger.error(f"Unkown schleuse state received: {newstate}")
                        continue

                    self.state_last_updated_gauge.set_to_current_time()
                    for state_option in SchleuseStateEnum:
                        self.state_gauge.labels(state_option.value).set(
                            1 if self.state == state_option else 0
                        )

                    async with self._state_changed:
                        self._state_changed.notify_all()


async def wait_for_schleuse_state(
    callback: Callable[[SchleuseStateEnum], Coroutine[Any, Any, None]],
    api_base_url: str | None = None,
):
    if api_base_url is None:
        api_base_url = os.getenv("MUCCC_API_BASE_URL", "http://localhost:8020")

    async with ClientSession() as session:
        while True:
            try:
                async with session.ws_connect(api_base_url + "/schleuse.json/ws") as ws:  # type: ignore
                    logger.info("connected to websocket")
                    async for msg in ws:
                        if msg.type == WSMsgType.TEXT:
                            state: str | None = msg.json()["state"]
                            if state is not None:
                                task = asyncio.create_task(callback(SchleuseStateEnum(state)))
                                background_tasks.add(task)
                                task.add_done_callback(background_tasks.discard)
                        elif msg.type == WSMsgType.ERROR:
                            break
                logger.warning("disconnected from websocket")
            except ClientConnectorError:
                logger.error("can't connect to websocket")
            except WSServerHandshakeError:
                logger.error("handshake error with websocket")
            except Exception as e:  # noqa: BLE001
                logger.error(f"unknown exception: {e}")
            finally:
                await asyncio.sleep(5)
