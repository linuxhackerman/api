# pyright: reportMissingTypeStubs=false, reportUnknownMemberType=false

import json
import zoneinfo
from collections.abc import Iterator
from dataclasses import dataclass
from datetime import datetime, timedelta
from os import environ as env
from typing import Any

import icalendar
from dokuwiki import DokuWiki
from loguru import logger

tz = zoneinfo.ZoneInfo("Europe/Berlin")


def wiki_url(base: str, page_id: str) -> str:
    return f"{base}/{page_id}"


@dataclass
class Event:
    id: str
    name: str
    start: datetime
    end: datetime
    description: str | None
    public: bool
    location: list[str] | None
    organizer: str | None
    language: str | None
    url: str

    def duration(self):
        "returns duration of event in minutes"
        return (self.end - self.start).seconds / 60

    @staticmethod
    def from_dict(obj: dict[str, str], base: str) -> "Event":
        page_id = obj.get("%pageid%")
        id = f'{page_id}#{obj.get("%rowid%")}'
        name = obj.get("event.name", "NOEVENT")

        # Add timezone information to datetimes: Europe/Berlin
        start = datetime.strptime(obj.get("event.startDate", ""), "%Y-%m-%d %H:%M").replace(
            tzinfo=tz
        )
        endDate = obj.get("event.endDate")
        try:
            end = datetime.strptime(
                endDate if endDate is not None else "", "%Y-%m-%d %H:%M"
            ).replace(tzinfo=tz)
        except ValueError:
            end = start + timedelta(hours=2)

        description = obj.get("event.description")
        public = obj.get("event.public") == "public"
        location = obj.get("event.location")

        organizer = obj.get("event.organizer")
        language = obj.get("event.language")
        url = wiki_url(base, id)

        return Event(
            id,
            name,
            start,
            end,
            description,
            public,
            location,
            organizer,
            language,
            url,
        )

    def to_ical(self) -> icalendar.Event:
        event = icalendar.Event()
        event.add("summary", self.name)
        logger.debug(f"Processing {self.name}")

        # Add dtstart and dtend to event
        event.add("dtstart", self.start)
        event.add("dtend", self.end)

        description = f"\n\n{self.description}" if self.description is not None else ""
        if self.public:
            event.add("description", "Public" + description)
        else:
            event.add("description", "Members" + description)

        if self.location is not None:
            event.add("location", self.location)

        event.add("url", self.url)
        event.add("uid", self.url.replace("https://", "").replace(":", "."))
        return event

    def to_dict(self) -> dict[str, Any]:
        return {
            "name": self.name,
            "start": self.start.isoformat(),
            "duration": self.duration(),
            "description": self.description,
            "is_public": self.public,
            "rooms": self.location,
            "organizer": self.organizer,
            "language": self.language,
            "url": self.url,
        }

    def __str__(self):
        return json.dumps(self.to_dict(), indent=2)


@dataclass
class EventFilter:
    public: bool | None = None
    upcoming: bool = False

    def values(self) -> list[dict[str, str]]:
        query: list[str] = []
        if self.upcoming:
            now = datetime.now(tz=tz).strftime("%Y-%m-%d %H:%M")
            query.append("event.startDate >= " + now)
        if self.public:
            query.append("public != ")

        return [{"condition": x, "logic": "and"} for x in query]

    def __iter__(self):
        return self.values().__iter__()


class Wiki(DokuWiki):
    def __init__(self, base: str = "https://wiki.muc.ccc.de", user: str = "api-bot"):
        self.base = base
        super().__init__(
            self.base,
            env.get("WIKI_USER", user),
            env.get("WIKI_PASSWORD", "xxx"),
            cookieAuth=True,
        )

    # https://www.dokuwiki.org/plugin:struct:remote_api
    # https://python-dokuwiki.readthedocs.io/en/latest/#structs

    def events(self, filter: EventFilter | list[dict[str, str]]):  # TODO List[AggregationFilter]
        for event in self.structs.get_aggregation_data(  # type: ignore
            schemas=["event"],
            columns=[
                "%rowid%",
                "%pageid%",
                "name",
                "startDate",
                "endDate",
                "description",
                "public",
                "location",
                "organizer",
                "language",
            ],
            data_filter=filter.values() if type(filter) == EventFilter else filter,
            sort="event.startDate",
        ):
            yield Event.from_dict(event, self.base)  # type: ignore

    def upcoming_events(self) -> Iterator[Event]:
        return self.events(EventFilter(upcoming=True))

    def public_events(self) -> Iterator[Event]:
        return self.events(EventFilter(public=True))

    def todays_events(self) -> Iterator[Event]:
        today = datetime.now(tz=tz).today()
        tomorrow = today + timedelta(days=1)

        return self.events([
            {"condition": "event.startDate >= " + today.strftime("%Y-%m-%d")},
            {
                "logic": "and",
                "condition": "event.startDate < " + tomorrow.strftime("%Y-%m-%d"),
            },
        ])

    def ical(self, filter: EventFilter, name: str = "Kalenderexport") -> bytes:
        cal = icalendar.Calendar()
        cal.add("prodid", f"-//api.muc.ccc.de {name}//")
        cal.add("version", "2.0")

        for event in self.events(filter):
            cal.add_component(event.to_ical())

        return cal.to_ical()


if __name__ == "__main__":
    wiki = Wiki("https://wiki.muc.ccc.de")

    for event in wiki.public_events():
        print(event)

    for event in wiki.todays_events():
        print(event)
