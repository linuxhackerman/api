{
  description = "MuCCC API";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      poetry2nix,
      ...
    }:
    {
      overlays.default = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlays.default
        (
          final: prev:
          let
            python = final.python311;
            overrides = prev.poetry2nix.overrides.withDefaults (
              self: super: {
                fastapi-cache2 = super.fastapi-cache2.overrideAttrs (old: {
                  nativeBuildInputs = old.nativeBuildInputs ++ [ self.poetry-core ];
                  postPatch = ''
                    substituteInPlace pyproject.toml \
                    --replace poetry.masonry.api poetry.core.masonry.api \
                    --replace "poetry>=" "poetry-core>="
                  '';
                });
                pydle = super.pydle.overrideAttrs (old: {
                  nativeBuildInputs = old.nativeBuildInputs ++ [ self.poetry ];
                });
                dokuwiki = super.dokuwiki.overrideAttrs (old: {
                  nativeBuildInputs = old.nativeBuildInputs ++ [ self.setuptools ];
                });
              }
            );
          in
          {
            muccc-api = prev.poetry2nix.mkPoetryApplication {
              inherit python overrides;
              projectDir = prev.poetry2nix.cleanPythonSources { src = ./.; };
            };

            muccc-api-dev = final.poetry2nix.mkPoetryEnv {
              inherit python overrides;
              pyproject = ./pyproject.toml;
              poetrylock = ./poetry.lock;
              extraPackages = ps: [ ps.ipython ];
            };
          }
        )
      ];
    }
    // (flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlays.default ];
        };
      in
      {
        packages = {
          default = pkgs.muccc-api;

          docker-image = pkgs.dockerTools.buildImage {
            name = pkgs.muccc-api.pname;
            tag = "latest";
            contents = [ pkgs.muccc-api ];
            config = {
              Cmd = [ "muccc-api" ];
            };
          };
        };

        formatter = pkgs.nixfmt-rfc-style;

        devShells.default = pkgs.muccc-api-dev.env.overrideAttrs (oldAttrs: {
          nativeBuildInputs =
            oldAttrs.nativeBuildInputs
            ++ (with pkgs; [
              nil
              poetry
            ]);
          shellHook = ''
            export POETRY_HOME=${pkgs.poetry}
            export POETRY_BINARY=${pkgs.poetry}/bin/poetry
            export POETRY_VIRTUALENVS_IN_PROJECT=true
            export PYTHONPATH=${pkgs.muccc-api-dev}/${pkgs.muccc-api-dev.sitePackages}
            unset SOURCE_DATE_EPOCH
          '';
        });
      }
    ));
}
