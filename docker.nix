{
  nixpkgs ? <nixpkgs>,
}:
let
  pkgs = import nixpkgs { };
in
pkgs.dockerTools.buildImage {
  name = "muccc-api";
  tag = "latest";
  contents = [ (import ./. { }) ];
  config = {
    Cmd = [
      "uvicorn"
      "muccc_api.app:app"
      "--host"
      "0.0.0.0"
      "--port"
      "80"
    ];
  };
}
